#include "contas.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define atrasar() sleep(ATRASO)

pthread_mutex_t trincos_contas[NUM_CONTAS];
		     
int contasSaldos[NUM_CONTAS];
int flag = 0;

void inicializarTrincos(){
  for (int i = 0; i < NUM_CONTAS; i++) {
    pthread_mutex_init(&trincos_contas[i], NULL);
  }
}

int contaExiste(int idConta) {
  return (idConta > 0 && idConta <= NUM_CONTAS);
}

void inicializarContas() {
  int i;
  for (i=0; i<NUM_CONTAS; i++)
    contasSaldos[i] = 0;
}

int debitar(int idConta, int valor) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  pthread_mutex_lock(&trincos_contas[idConta-1]);
  if (contasSaldos[idConta - 1] < valor){
    pthread_mutex_unlock(&trincos_contas[idConta-1]);
    return -1;
  }
  atrasar();
  contasSaldos[idConta - 1] -= valor;
  pthread_mutex_unlock(&trincos_contas[idConta-1]);
  return 0;
}

int creditar(int idConta, int valor) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  pthread_mutex_lock(&trincos_contas[idConta-1]);
  contasSaldos[idConta - 1] += valor;
  pthread_mutex_unlock(&trincos_contas[idConta-1]);
  return 0;
}

int lerSaldo(int idConta) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  pthread_mutex_lock(&trincos_contas[idConta-1]);
  int saldo = contasSaldos[idConta - 1];
  pthread_mutex_unlock(&trincos_contas[idConta-1]);
  return saldo;
}

void simular(int numAnos) {
    int ano, i, max;
    
    // Imprime as estimativas até ao numAnos
    for (ano=0; ano <= numAnos; ano++) {
        printf("\nSIMULACAO: Ano %d\n",ano);
        printf("=================\n");
        for (i=0; lerSaldo(i+1) != 0 && i < 10; i++) { // Imprime até encontrar conta a zero ou percorrer todas as contas
            printf("Conta %d, Saldo %d\n", i+1, lerSaldo(i+1));
            max = intMax(lerSaldo(i+1) * (1 + TAXAJURO) - CUSTOMANUTENCAO,0);
            if (max != 0) {
                creditar(i+1,lerSaldo(i+1) * TAXAJURO);
                debitar(i+1, CUSTOMANUTENCAO);
            }
        }
        if (i < 10) // Caso nao tenham sido imprimidas todas as contas
            printf("Conta %d, Saldo %d\n", i+1, 0); // Imprime a primeira conta a zero
        if (flag == 1) { // Verifica se a flag foi activada atraves do sinal SIGUSR1
            printf("Simulacao terminada por signal\n");
            exit(3);
        }
    }
}

/*
 intMax:
 Recebe dois inteiros e devolve o maior
 */
int intMax(int a,int b) {
    if (a >= b)
        return a;
    else
        return b;
}
/*
 alteraFlag:
 funcao chamada quando e' recebido o sinal SIGUR1 e ativa a flag
*/
void alteraFlag() {
    flag = 1;
}


int transferir(int idContaOrigem, int idContaDestino, int valor) {
  atrasar();
  if ((!contaExiste(idContaOrigem)) && (!contaExiste(idContaDestino)))
    return -1;
  if (idContaOrigem < idContaDestino) { // fecha o trinco do idConta menor primeiro e depois o maior
    pthread_mutex_lock(&trincos_contas[idContaOrigem-1]);
    pthread_mutex_lock(&trincos_contas[idContaDestino-1]);
  }
  else {
    pthread_mutex_lock(&trincos_contas[idContaDestino-1]);
    pthread_mutex_lock(&trincos_contas[idContaOrigem-1]);
  }

  if(debitarTransferir(idContaOrigem,valor) < 0) {
    pthread_mutex_unlock(&trincos_contas[idContaOrigem-1]);
    pthread_mutex_unlock(&trincos_contas[idContaDestino-1]);
    return -1;
  }
  creditarTransferir(idContaDestino,valor);
  pthread_mutex_unlock(&trincos_contas[idContaOrigem-1]);
  pthread_mutex_unlock(&trincos_contas[idContaDestino-1]);
  return 0;
}

int debitarTransferir(int idConta, int valor) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  if (contasSaldos[idConta - 1] < valor){
    return -1;
  }
  atrasar();
  contasSaldos[idConta - 1] -= valor;
  return 0;
}

int creditarTransferir(int idConta, int valor) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  contasSaldos[idConta - 1] += valor;
  return 0;
}

int sizeofstr(char* string) {
  int size = 0;
  for (int i = 0; string[i] != '\0'; i++) {
    size++;
  }
  
  return size;
}