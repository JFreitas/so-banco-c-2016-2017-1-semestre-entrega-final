/*
// Projeto SO - exercicio 1, version 1
// Sistemas Operativos, DEI/IST/ULisboa 2016-17
// Grupo 73 - Alameda
*/

#include "commandlinereader.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>


#define COMANDO_DEBITAR "debitar"
#define COMANDO_CREDITAR "creditar"
#define COMANDO_LER_SALDO "lerSaldo"
#define COMANDO_SIMULAR "simular"
#define COMANDO_SAIR "sair"
#define COMANDO_AGORA "agora"
#define COMANDO_TRANSFERIR "transferir"
#define COMANDO_TERMINAR "sair-terminal"

#define MAXARGS 4
#define BUFFER_SIZE 100

#define NUM_TRABALHADORAS 3
#define CMD_BUFFER_DIM 6
#define OP_LERSALDO 0
#define OP_CREDITAR 1
#define OP_DEBITAR 2
#define OP_NENHUM -1
#define OP_TRANSFERIR 3
#define OP_SAIR_NORMAL 4
#define OP_SAIR_AGORA 5
#define OP_SAIR_TERMINAL 6
#define OP_SIMULAR 7

#define STDOUT 1

typedef struct
{
    int operacao;
    int idConta;
    int idContaDestino;
    int valor;
    int pid;
} comando_t;

void signal_recebido() {
	printf("O i-banco está fechado!\n");
	exit(0);
} 
int main (int argc, char** argv) {
	signal(SIGPIPE, signal_recebido); // caso o i-banco-servidor esteja fechado chama a func signal_recebido
	int pipebanco, pipeterminal;
    if ((pipebanco = open(argv[1], O_WRONLY)) < 0) {
        exit(-1);
    }

	char *args[MAXARGS + 1];
	char buffer[BUFFER_SIZE];
	char filename[BUFFER_SIZE]; // nome do pipe criado pelo terminal para receber as mensagens
	char print[BUFFER_SIZE]; // string que guarda o print da operacao efetuada
	comando_t comando;

	snprintf(filename,BUFFER_SIZE, "terminal-pipe-%d",getpid());
	unlink(filename); //se houver ficheiro com nome igual apaga-o
	if (mkfifo (filename,0777) < 0) {
		exit(-1);
	}
	
	time_t start_t, end_t;
	double diff_t;

	comando.pid = getpid();

	while (1) {
        int numargs;
        numargs = readLineArguments(args, MAXARGS+1, buffer, BUFFER_SIZE);
        /* EOF (end of file) do stdin ou comando "sair" */
        if (numargs < 0 ||
	        (numargs > 0 && (strcmp(args[0], COMANDO_SAIR) == 0))) {
           
            
            if (numargs > 1 && (strcmp(args[1], COMANDO_AGORA) == 0)) {
            	comando.operacao = OP_SAIR_AGORA;
                write(pipebanco,&comando,sizeof(comando)); // envia o comando sair agora para o i-banco
            }
            else {
                comando.operacao = OP_SAIR_NORMAL;
            write(pipebanco,&comando,sizeof(comando)); // envia o comando sair para o i-banco

        }
    }
    
    else if (numargs == 0)
        /* Nenhum argumento; ignora e volta a pedir */
        continue;
            
    /* Debitar */
    else if (strcmp(args[0], COMANDO_DEBITAR) == 0) {
        int idConta, valor;
        if (numargs < 3) {
            printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_DEBITAR);
	        continue;
        }

        idConta = atoi(args[1]);
        valor = atoi(args[2]);

        comando.operacao = OP_DEBITAR;
        comando.idConta = idConta;
        comando.valor = valor;
        write(pipebanco,&comando,sizeof(comando)); // envia o comando debitar para o i-banco
        time(&start_t);
        if ((pipeterminal = open(filename, O_RDONLY)) < 0)  { // abre o pipe criado pelo terminal 
			exit(-1);
		}
       	read(pipeterminal,&print,sizeof(print)); // recebe o print da operacao executada
       	time(&end_t);
       	close(pipeterminal); // fecha o pipe criado pelo terminal
       	printf("%s", print);
       	diff_t = difftime(end_t, start_t); // calcula o tempo da operacao
        printf("Tempo: %f\n", diff_t );
    }
    /* Creditar */
    else if (strcmp(args[0], COMANDO_CREDITAR) == 0) {
        int idConta, valor;
        if (numargs < 3) {
            printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_CREDITAR);
            continue;
        }

        idConta = atoi(args[1]);
        valor = atoi(args[2]);

        comando.operacao = OP_CREDITAR;
        comando.idConta = idConta;
        comando.valor = valor;
        write(pipebanco,&comando,sizeof(comando)); // envia o comando creditar para o i-banco
        time(&start_t);
        if ((pipeterminal = open(filename, O_RDONLY)) < 0)  { // abre o pipe criado pelo terminal 
			exit(-1);
		}
       	read(pipeterminal,&print,sizeof(print)); // recebe o print da operacao executada
       	time(&end_t);
       	close(pipeterminal); // fecha o pipe criado pelo terminal
       	printf("%s", print);
       	diff_t = difftime(end_t, start_t); // calcula o tempo da operacao
       	printf("Tempo: %f\n", diff_t );
        
    }
    /* Ler Saldo */
    else if (strcmp(args[0], COMANDO_LER_SALDO) == 0) {
        int idConta;

         if (numargs < 2) {
            printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_LER_SALDO);
            continue;
        }
        idConta = atoi(args[1]);
        
        comando.operacao = OP_LERSALDO;
        comando.idConta = idConta;
        comando.valor = 0;
        write(pipebanco,&comando,sizeof(comando)); // envia o comando lerSaldo para o i-banco
        time(&start_t);
        if ((pipeterminal = open(filename, O_RDONLY)) < 0)  { // abre o pipe criado pelo terminal 
			exit(-1);
		}
       	read(pipeterminal,&print,sizeof(print)); // recebe o print da operacao executada
       	time(&end_t);
       	close(pipeterminal); // fecha o pipe criado pelo terminal
       	printf("%s", print);
       	diff_t = difftime(end_t, start_t);
       	printf("Tempo: %f\n", diff_t );
        
    }
    /* Transferir */
    else if (strcmp(args[0], COMANDO_TRANSFERIR) == 0) {
        int idContaOrigem, idContaDestino, valor;

        if (numargs < 4) {
            printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_TRANSFERIR);
            continue;
        }
        idContaOrigem = atoi(args[1]);
        idContaDestino = atoi(args[2]);
        valor = atoi(args[3]);

        comando.operacao = OP_TRANSFERIR;
        comando.idConta = idContaOrigem;
        comando.idContaDestino = idContaDestino;
        comando.valor = valor;
        write(pipebanco,&comando,sizeof(comando)); // envia o comando transferir para o i-banco
        time(&start_t);
        if ((pipeterminal = open(filename, O_RDONLY)) < 0)  { // abre o pipe criado pelo terminal 
			exit(-1);
		}
       	read(pipeterminal,&print,sizeof(print)); // recebe o print da operacao executada
       	time(&end_t);
       	close(pipeterminal); // fecha o pipe criado pelo terminal
       	printf("%s", print);
       	diff_t = difftime(end_t, start_t);
       	printf("Tempo: %f\n", diff_t );

    }
    /* Simular */
    else if (strcmp(args[0], COMANDO_SIMULAR ) == 0) {
        int numAnos;
        
        if (numargs < 2) {
            printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_SIMULAR);
            continue;
        }
        numAnos = atoi(args[1]);
        if (numAnos < 0) //numAnos não pode ser negativo
            printf("%s(%d): Erro.\n\n", COMANDO_SIMULAR, numAnos);
        else{
        	comando.valor = numAnos;
 			comando.operacao = OP_SIMULAR;
            write(pipebanco,&comando,sizeof(comando)); // envia o comando simular para o i-banco
        }
    }
    else if (strcmp(args[0], COMANDO_TERMINAR) == 0) {
    	close(pipebanco); // fecha o pipe criado pelo i-banco
    	close(pipeterminal); // fecha o pipe criado pelo terminal
    	unlink(filename);
    	exit(EXIT_SUCCESS);
    }
    else {
      printf("Comando desconhecido. Tente de novo.\n");
    }
  } 
}


