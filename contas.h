/*
// Operações sobre contas, versao 1
// Sistemas Operativos, DEI/IST/ULisboa 2016-17
*/

#ifndef CONTAS_H
#define CONTAS_H
 
#define NUM_CONTAS 10
#define TAXAJURO 0.1
#define CUSTOMANUTENCAO 1

#define ATRASO 1

void inicializarContas();
int contaExiste(int idConta);
int debitar(int idConta, int valor);
int creditar(int idConta, int valor);
int debitarTransferir(int idConta, int valor);
int creditarTransferir(int idConta, int valor);
int lerSaldo(int idConta);
void simular(int numAnos);
int intMax(int a,int b);
void alteraFlag();
int transferir(int idContaOrigem, int idContaDestino, int valor);
int sizeofstr(char* string);

#endif
