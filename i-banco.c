/*
// Projeto SO - exercicio 1, version 1
// Sistemas Operativos, DEI/IST/ULisboa 2016-17
// Grupo 73 - Alameda
*/

#include "commandlinereader.h"
#include "contas.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>


#define COMANDO_DEBITAR "debitar"
#define COMANDO_CREDITAR "creditar"
#define COMANDO_LER_SALDO "lerSaldo"
#define COMANDO_SIMULAR "simular"
#define COMANDO_SAIR "sair"
#define COMANDO_AGORA "agora"
#define COMANDO_TRANSFERIR "transferir"

#define MAXARGS 4
#define BUFFER_SIZE 100

#define NUM_TRABALHADORAS 3
#define CMD_BUFFER_DIM 6
#define OP_LERSALDO 0
#define OP_CREDITAR 1
#define OP_DEBITAR 2
#define OP_NENHUM -1
#define OP_SAIR -2
#define OP_TRANSFERIR 3
#define OP_SAIR_NORMAL 4
#define OP_SAIR_AGORA 5
#define OP_SAIR_TERMINAL 6
#define OP_SIMULAR 7

#define STDOUT 1

typedef struct
{
	int operacao;
	int idConta;
	int idContaDestino;
	int valor;
	int pid;
} comando_t;


comando_t cmd_buffer[CMD_BUFFER_DIM]; // buffer circular
int buff_write_idx = 0, buff_read_idx = 0;

sem_t semBuffer, semRun;
pthread_mutex_t trincos[CMD_BUFFER_DIM], trinco_buff_read;

int num_tarefas = 0;
pthread_mutex_t trinco_num_tarefas;
pthread_cond_t var_cond_simular;

int file_descripter;

void insere_buffer(comando_t comando) {
	while(1) {
		sem_wait(&semBuffer); // espera se o buffer estiver cheio
		pthread_mutex_lock(&trincos[buff_write_idx]);
		if (cmd_buffer[buff_write_idx].operacao == OP_NENHUM) {
			cmd_buffer[buff_write_idx] = comando;
			pthread_mutex_unlock(&trincos[buff_write_idx]);
			buff_write_idx = (buff_write_idx + 1) % CMD_BUFFER_DIM;
			pthread_mutex_lock(&trinco_num_tarefas);
			num_tarefas++;
			pthread_mutex_unlock(&trinco_num_tarefas);
			sem_post(&semRun);
			return;
		}
		pthread_mutex_unlock(&trincos[buff_write_idx]);
		buff_write_idx = (buff_write_idx + 1) % CMD_BUFFER_DIM;
	}
}

void *run_comando() {
	int saldo, pipeterminal;
	char logstr[BUFFER_SIZE], print[BUFFER_SIZE], pipename[BUFFER_SIZE];
	pthread_t self = pthread_self();
	while (1) {
		pthread_mutex_lock(&trinco_buff_read);
		int pos = buff_read_idx;
		buff_read_idx = (buff_read_idx + 1) % CMD_BUFFER_DIM;
		pthread_mutex_unlock(&trinco_buff_read);
		sem_wait(&semRun);
		pthread_mutex_lock(&trincos[pos]);
		if (cmd_buffer[pos].operacao != OP_NENHUM) {
			switch (cmd_buffer[pos].operacao) {
			case OP_LERSALDO:
					snprintf(pipename,BUFFER_SIZE, "terminal-pipe-%d",cmd_buffer[pos].pid);
					if ((pipeterminal = open(pipename, O_WRONLY)) < 0)  {
						exit(-1);
					}
					saldo = lerSaldo(cmd_buffer[pos].idConta);
					if (saldo < 0)
						snprintf(print, BUFFER_SIZE,"%s(%d): Erro.\n", COMANDO_LER_SALDO, cmd_buffer[pos].idConta);
					else
						snprintf(print, BUFFER_SIZE,"%s(%d): O saldo da conta é %d.\n", COMANDO_LER_SALDO, cmd_buffer[pos].idConta, saldo);
					write(pipeterminal,&print,sizeof(print)); // envia para o pipe correspondente ao terminal que enviou o resultado da operacao (mensagem)
					snprintf( logstr, BUFFER_SIZE, "%lu: lerSaldo %d\n", self, cmd_buffer[pos].idConta);
					write(file_descripter, logstr, sizeofstr(logstr)); // escreve no log a operacao introduzida
					close(pipeterminal);
				break;
			case OP_CREDITAR:
					snprintf(pipename,BUFFER_SIZE, "terminal-pipe-%d",cmd_buffer[pos].pid);
					if ((pipeterminal = open(pipename, O_WRONLY)) < 0)  {
						exit(-1);
					}
					if (creditar (cmd_buffer[pos].idConta, cmd_buffer[pos].valor) < 0)
						snprintf(print, BUFFER_SIZE,"%s(%d, %d): Erro\n", COMANDO_CREDITAR, cmd_buffer[pos].idConta, cmd_buffer[pos].valor);
					else
						snprintf(print, BUFFER_SIZE,"%s(%d, %d): OK\n", COMANDO_CREDITAR, cmd_buffer[pos].idConta, cmd_buffer[pos].valor);
					write(pipeterminal,&print,sizeof(print));
					snprintf( logstr, BUFFER_SIZE, "%lu: creditar %d %d\n", self, cmd_buffer[pos].idConta, cmd_buffer[pos].valor);
					write(file_descripter, logstr, sizeofstr(logstr));
					close(pipeterminal);
				break;
			case OP_DEBITAR:
					snprintf(pipename,BUFFER_SIZE, "terminal-pipe-%d",cmd_buffer[pos].pid);
					if ((pipeterminal = open(pipename, O_WRONLY)) < 0)  {
						exit(-1);
					}
					if (debitar (cmd_buffer[pos].idConta, cmd_buffer[pos].valor) < 0)
						snprintf(print, BUFFER_SIZE,"%s(%d, %d): Erro\n", COMANDO_DEBITAR, cmd_buffer[pos].idConta, cmd_buffer[pos].valor);
					else
						snprintf(print, BUFFER_SIZE,"%s(%d, %d): OK\n", COMANDO_DEBITAR, cmd_buffer[pos].idConta, cmd_buffer[pos].valor);
					write(pipeterminal,&print,sizeof(print));
					snprintf( logstr, BUFFER_SIZE, "%lu: debitar %d %d\n", self, cmd_buffer[pos].idConta, cmd_buffer[pos].valor);
					write(file_descripter, logstr, sizeofstr(logstr));
					close(pipeterminal);
				break;
			case OP_TRANSFERIR:
				snprintf(pipename,BUFFER_SIZE, "terminal-pipe-%d",cmd_buffer[pos].pid);
					if ((pipeterminal = open(pipename, O_WRONLY)) < 0)  {
						exit(-1);
					}
				if (cmd_buffer[pos].idConta == cmd_buffer[pos].idContaDestino || transferir(cmd_buffer[pos].idConta,cmd_buffer[pos].idContaDestino, cmd_buffer[pos].valor) < 0)
					snprintf(print, BUFFER_SIZE,"Erro ao transferir %d da conta %d para a conta %d.\n", cmd_buffer[pos].valor, cmd_buffer[pos].idConta, cmd_buffer[pos].idContaDestino);
				else
					snprintf(print, BUFFER_SIZE,"transferir(%d, %d, %d): OK\n", cmd_buffer[pos].idConta, cmd_buffer[pos].idContaDestino, cmd_buffer[pos].valor);
				write(pipeterminal,&print,sizeof(print));
				snprintf( logstr, BUFFER_SIZE, "%lu: transferir %d %d %d\n", self, cmd_buffer[pos].idConta, cmd_buffer[pos].idContaDestino  ,cmd_buffer[pos].valor);
				write(file_descripter, logstr, sizeofstr(logstr));
				close(pipeterminal);
				break;
			case OP_SAIR:
					pthread_mutex_unlock(&trincos[pos]);
					pthread_exit(NULL);
					break;
			default:
				break;
			}
			cmd_buffer[pos].operacao = OP_NENHUM;
			pthread_mutex_unlock(&trincos[pos]);
			pthread_mutex_lock(&trinco_num_tarefas);
			num_tarefas--;
			pthread_cond_signal(&var_cond_simular);
			pthread_mutex_unlock(&trinco_num_tarefas);
			sem_post(&semBuffer);
		}
		else {
			pthread_mutex_unlock(&trincos[pos]);
			sem_post(&semRun); 
		}        
	}
}


int main (int argc, char** argv) {
	
	pthread_t tid[NUM_TRABALHADORAS];

	
	signal(SIGUSR1, alteraFlag);
	inicializarContas();

	sem_init(&semBuffer,0,6);
	sem_init(&semRun,0,0);

	pthread_cond_init(&var_cond_simular, NULL);

	inicializarContas();

	pthread_mutex_init(&trinco_buff_read, NULL);
	
	/** Pipes **/

	unlink("i-banco-pipe");

	if (mkfifo ("i-banco-pipe",0777) < 0) {
		exit(-1);
	}

	int pipebanco;
	if ((pipebanco = open("i-banco-pipe", O_RDONLY)) < 0)  {
		exit(-1);
	}

	file_descripter = open("log.txt",O_WRONLY | O_CREAT | O_TRUNC, 0666);


	for (int i = 0; i < CMD_BUFFER_DIM; i++) {
		cmd_buffer[i].operacao = -1;
		pthread_mutex_init(&trincos[i], NULL);
	}

	for (int i = 0; i < NUM_TRABALHADORAS; i++) {
		pthread_create(&tid[i],0,run_comando,NULL);
	}
	printf("Bem-vinda/o ao i-banco\n\n");
	int n, pid2, status;
	comando_t comando;
	while(1) {
		n = read(pipebanco,&comando,sizeof(comando));
		if (n > 0) {
			switch(comando.operacao) {
				case OP_SIMULAR:
					pthread_mutex_lock(&trinco_num_tarefas);
					while(num_tarefas != 0){
						pthread_cond_wait(&var_cond_simular, &trinco_num_tarefas);
					}
					int pid;
					pid = fork();
					if (pid == -1){
						printf("Failed to fork process 1\n");
						exit(1);
					}
					if (pid == 0){
						char filename[BUFFER_SIZE];
						snprintf(filename, BUFFER_SIZE, "i-banco-sim-%d.txt", getpid());
						int fd = open(filename,O_WRONLY | O_CREAT | O_TRUNC, 0666); // cria ou se existe ficheiro trunca o seu tamanho para zero
						close(STDOUT);
						dup(fd); // escrever no STDOUT substituido por escrever no ficheiro i-banco-sim-pid
						simular(comando.valor); // escreve no ficheiro
						pthread_mutex_unlock(&trinco_num_tarefas);
						close(fd);
						exit(3);
					}
					else {
						pthread_mutex_unlock(&trinco_num_tarefas);
						continue;
					}
					break;
				case OP_SAIR_NORMAL:
					
					pid2 = 0;

					printf("i-banco vai terminar.\n--\n");
				
					while((pid2 = wait(&status)) > 0) {
						if(WIFEXITED(status) > 0)
							printf("FILHO TERMINADO (PID=%d; terminou normalmente)\n",pid2);
						else if (WIFSIGNALED(status) > 0) {
							printf("FILHO TERMINADO (PID=%d; terminou abruptamente)\n",pid2);
						}
					}
				
					printf("--\ni-banco vai terminar.\n");
					while(1) {
						int boolean=1;
						for (int i=0; i < CMD_BUFFER_DIM; i++) {
							pthread_mutex_lock(&trincos[i]);
							boolean = boolean && (cmd_buffer[i].operacao == -1);
							pthread_mutex_unlock(&trincos[i]);
						}
						if(boolean){
							comando.operacao = OP_SAIR;
							comando.idConta = 0;
							comando.valor = 0;
							for (int i=0; i < NUM_TRABALHADORAS; i++) {
								insere_buffer(comando);
							}
							for (int i = 0; i < NUM_TRABALHADORAS; i++){
								pthread_join(tid[i],NULL);
							}
							close(file_descripter);
							close(pipebanco);
							unlink("i-banco-pipe");
							exit(EXIT_SUCCESS);
						}
					}
					break;
				case OP_SAIR_AGORA:
					/* Sair agora */
                	kill(0,SIGUSR1); /* Envia sinal aos filhos para encerrarem apos terminarem 
					de imprimir o ano */
                	while((pid2 = wait(&status)) > 0) {
                    	// Espera que todos os filhos tenham terminado
                	}
                	while(1) {
						int boolean=1;
						for (int i=0; i < CMD_BUFFER_DIM; i++) {
							pthread_mutex_lock(&trincos[i]);
							boolean = boolean && (cmd_buffer[i].operacao == -1);
							pthread_mutex_unlock(&trincos[i]);
						}
						if(boolean){
							comando.operacao = OP_SAIR;
							comando.idConta = 0;
							comando.valor = 0;
							for (int i=0; i < NUM_TRABALHADORAS; i++) {
								insere_buffer(comando);
							}
							for (int i = 0; i < NUM_TRABALHADORAS; i++){
								pthread_join(tid[i],NULL);
							}
							close(file_descripter);
							close(pipebanco);
							unlink("i-banco-pipe");
							exit(EXIT_SUCCESS);
						}
					}
					break;
				default: // resto das operacoes
					insere_buffer(comando);
					break;
			}
		}
	}
}