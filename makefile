CFLAGS = -Wall -g -pedantic

i-banco: commandlinereader.o contas.o i-banco.o i-banco-terminal.o
	gcc -o i-banco commandlinereader.o contas.o i-banco.o -lpthread
	gcc -o i-banco-terminal commandlinereader.o i-banco-terminal.o -lpthread

commandlinereader.o: commandlinereader.c commandlinereader.h
	gcc $(CFLAGS) -c commandlinereader.c

contas.o: contas.c contas.h
	gcc $(CFLAGS) -c contas.c

i-banco-terminal.o: i-banco-terminal.c commandlinereader.h
	gcc $(CFLAGS) -c i-banco-terminal.c

i-banco.o: i-banco.c commandlinereader.h contas.h
	gcc $(CFLAGS) -c i-banco.c

clean:
	rm -f *.o i-banco i-banco-terminal
